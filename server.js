const express = require('express');

const app = express();

app.get('/', (req,res) => res.send('api running'))

const PORT =  8000;


app.listen(PORT, () => (console.log(`Port is listening in ${PORT}`)));